module.exports = {
  title: "Your mission",
  head: [
    ['meta', { 
      name: 'viewport', 
      content: 'width=device-width, initial-scale=1, shrink-to-fit=no' 
    }],
    ['link', { rel: 'shortcut icon', href: '/favicon.ico' }],
    ['link', { rel: 'stylesheet', href: '/css/bulma.css' }],
    ['link', { rel: 'stylesheet', href: '/css/main.css' }]
//    ['script', { src: '/js/vue.min.js' }]
  ]
}

