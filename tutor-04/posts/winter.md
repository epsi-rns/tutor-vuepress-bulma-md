---
kind    : Post
title   : Surviving White Winter
date    : 2019-02-11
categories: [story]
tags    : [mission, impossible]
---

It was a frozen winter in cold war era.
We were two lazy men, a sleepy boy, two long haired women,
a husky with attitude, and two shotguns.
After three weeks, we finally configure herbstluftwm.

But we lost our beloved husky before we finally made it.
Now, every january, we remember our husky,
that helped all of us to survive.
